<?php

class Balticode_Dpd_Controller_Adminhtml_Abstract extends Mage_Adminhtml_Controller_Action
{
    /**
     * Collect post arguments
     *
     * @param Key of array
     * @return Array
     */
    protected function collectPostData($post_key = null)
    {
        return $this->getRequest()->getPost($post_key);
    }

    protected function collectSelectedOrders()
    {
        $orderIds = $this->collectPostData('order_ids');
        if (!is_array($orderIds)) {
            $orderIds = array($orderIds);
        }
        return $orderIds;
    }

    protected function getCurrentViewOrder()
    {
        $orderIds = $this->collectPostData('order');
        if (!is_array($orderIds)) {
            $orderIds = array($orderIds);
        }
        return $orderIds;
    }

    protected function collectOrders()
    {
        $orders = array();
        foreach ($this->collectSelectedOrders() as $orderId) {
            array_push($orders, $orderId); // grab from order list form
        }
        foreach ($this->getCurrentViewOrder() as $orderId) {
            array_push($orders, $orderId); // grab from order list form
        }

        $orders = array_values(array_filter(array_unique($orders)));

        return $orders;
    }

    protected function getOrdersIds($ordersIds = array())
    {
        if (!is_array($ordersIds)) { //If given null, or string
            $ordersIds = array($ordersIds); //Make it as array
        }
        $ordersIds = array_filter($ordersIds); //Null values has been trim

        if (!count($ordersIds)) { //If there was nothing left
            $ordersIds = $this->collectOrders();
        }

        return $ordersIds;
    }

    /**
     * Validating of orders is all correct
     *
     * @param  array $ordersIds - validating orders id
     * @return array | Boolean - array of available orders id; false - something wrong
     */
    public function validateOrders($ordersIds)
    {
        if (!is_array($ordersIds)) {
            $ordersIds = array($ordersIds);
        }
        if (!count($ordersIds)) {
            $this->registerError(Mage::helper('dpd')->__('Please select Orders!'));
            return false;
        }

        $availableOrders = array();

        $carrier = Mage::getModel('dpd/carrier');
        foreach ($ordersIds as $orderId) {
            $order = $carrier->getAvailableCarrier($orderId);
            if ($order != false) { //This order is DPD method?
                $availableOrders[] = $order;
            }
        }

        if (!count($availableOrders)) {
            $this->registerError(Mage::helper('dpd')->__('Wrong select Orders!'));
            return false;
        }

        //If logins has been difference
        if (!$this->loginValidation($availableOrders)) {
            return false;
        }

        return $availableOrders;
    }

    protected function loginValidation($orders)
    {
        foreach ($orders as $order) {
            $store_ids[] = $order->getStoreId();
        }
        //Orders store id's
        $store_ids = array_unique($store_ids);

        //Collect login data by store id
        foreach ($store_ids as $store_id) {
            $api_name[] = Mage::helper('dpd')->getConfigData('username', $store_id);
            $api_pass[] = Mage::helper('dpd')->getConfigData('password', $store_id);
            $id[] = Mage::helper('dpd')->getConfigData('id', $store_id);
            $api_url[] = Mage::helper('dpd')->getConfigData('api', $store_id);
        }

        //Make unique data
        $api_name = array_unique($api_name);
        $api_pass = array_unique($api_pass);
        $id = array_unique($id);
        $api_url = array_unique($api_url);
        $store_id = null;
        // Test logins
        if (count($api_name) == 1
            && count($api_pass) == 1
            && count($id) == 1
            && count($api_url) == 1
        ) {
            $this->api_name = Mage::helper('dpd')->getConfigData('username', $store_id);
            $this->api_pass = Mage::helper('dpd')->getConfigData('password', $store_id);
            $this->id = Mage::helper('dpd')->getConfigData('id', $store_id);
            $this->api_url = Mage::helper('dpd')->getConfigData('api', $store_id);
        } else {
            $message = Mage::helper('dpd')->__('Selected order has difference logins!');
            $this->registerError($message);
            return false;
        }
        return true;
    }

    protected function getParcelQuantity($order, $parcelQty)
    {
        $quantity = '1';
        if ($parcelQty == 'split' && $this->getParcelType($order) != 'PS') {
            $mpsType = Mage::helper('dpd')->getConfigData('mps_type');
            if ($mpsType == 'item') {
                $quantity = count($order->getAllVisibleItems());
            } else {
                $quantity = $order->getData('total_qty_ordered');
            }
        }
        if (is_numeric($parcelQty)) {
            $quantity = $parcelQty;
        }
        return $quantity;
    }

    /**
     * Return DPD shipping type
     *
     * @param  object - Order
     * @return string - DPD Shipping type, PS - ParcelStore, B2C - Business To Customer, D-COD-B2C...
     * for more info view in $this->$dpd_available_parcel_types
     */
    public function getParcelType($order)
    {
        $carrier = Mage::getModel('dpd/carrier');
        $method = $carrier->getMethod($order->getShippingMethod());
        $carrierMethod = $carrier->getModelClass($method);
        $parcelType = $carrierMethod->getType($order->getId());
        if ($parcelType !== false) {
            return $parcelType;
        } else {
            //Something wrong
            $message = Mage::helper('dpd')->__('Order:').' '.
                $order->getId().', '
                .Mage::helper('dpd')->__('something is wrong, cant find Parcel Type.');
            $this->registerWarning(Mage::helper('dpd')->__($message));
            return false;
        }
    }

    /**
     * Redirect to page from where come
     */
    protected function goBack()
    {
        $this->_redirectReferer();
    }

    //**************************************************************************
    //              Messages
    //**************************************************************************
    /**
     * Add error text to Array
     *
     * @param  string - Error Message
     * @return mix - Self class;
     */
    public function registerError($errorMessage)
    {
        Mage::helper('dpd')->registerError($errorMessage);
        return __CLASS__;
    }

    /**
     * Add Warning text to array
     *
     * @param  string -  Warning Messages
     * @return mix - self class;
     */
    public function registerWarning($warningMessage)
    {
        Mage::helper('dpd')->registerWarning($warningMessage);
        return __CLASS__;
    }
}
