<?php

class Balticode_Dpd_Model_Api_ParcelImport extends Balticode_Dpd_Model_Api_Abstract
{
    protected $interface = 'parcel_import.php';

    protected function fillParameters()
    {
        parent::fillParameters();
        foreach ($this->_params as $key => $value) {
            $this->parameters[$key] = $value;
        }

        return $this;
    }
}
