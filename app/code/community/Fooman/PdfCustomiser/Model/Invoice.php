<?php

/**
 * @author     Kristof Ringleff
 * @package    Fooman_PdfCustomiser
 * @copyright  Copyright (c) 2009 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Fooman_PdfCustomiser_Model_Invoice extends Fooman_PdfCustomiser_Model_Abstract
{
    const PDFCUSTOMISER_PDF_TYPE='invoice';

    /**
     * return type of pdf being rendered
     *
     * @return string
     */
    public function getPdfType()
    {
        return self::PDFCUSTOMISER_PDF_TYPE;
    }

    /**
     * Creates invoice pdf using the tcpdf library
     * pdf is either returned as object or sent to
     * the browser
     *
     * @param array  $invoicesGiven
     * @param array  $orderIds
     * @param null   $pdf
     * @param bool   $suppressOutput
     * @param string $outputFileName
     * @param null   $forceStoreId
     *
     * @access public
     *
     * @return bool|Fooman_PdfCustomiser_Model_Mypdf
     */
    public function renderPdf(
        $invoicesGiven = array(), $orderIds = array(), $pdf = null, $suppressOutput = false, $outputFileName = '',
        $forceStoreId = null,  $hideLogo = null
    ) {

        if (empty($pdf) && empty($invoicesGiven) && empty($orderIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('adminhtml')->__('There are no printable documents related to selected orders')
            );
            return false;
        }

        //we will be working through an array of orderIds later - fill it up if only invoices are given
        if (!empty($invoicesGiven)) {
            foreach ($invoicesGiven as $invoiceGiven) {
                $currentOrderId = $invoiceGiven->getOrder()->getId();
                $orderIds[$invoiceGiven->getId()] = $currentOrderId;
            }
        }

        $this->_beforeGetPdf();

        //need to get the store id from the first order to initialise pdf
        $storeId = Mage::getModel('sales/order')->load(current($orderIds))->getStoreId();

        //work with a new pdf or add to existing one
        if (empty($pdf)) {
            $pdf = $this->getMypdfModel($storeId);
        }

        $printedIncrements = array();
        foreach ($orderIds as $key => $orderId) {
            //load data

            $order = Mage::getModel('sales/order')->load($orderId);
            if (!empty($invoicesGiven)) {
                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                    ->addAttributeToSelect('*')
                    ->setOrderFilter($orderId)
                    ->addAttributeToFilter('entity_id', $key)
                    ->load();
            } else {
                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                    ->addAttributeToSelect('*')
                    ->setOrderFilter($orderId)
                    ->load();
            }

            //loop over invoices
            if ($invoices->getSize() > 0) {
                foreach ($invoices as $invoice) {
                    $i = $pdf->getNumPages();
                    if ($i > 0) {
                        $pdf->endPage();
                    }

                    // create new invoice helper
                    /* var $invoiceHelper Fooman_PdfCustomiser_Helper_Pdf_Invoice */
                    $invoiceHelper = Mage::helper('pdfcustomiser/pdf_invoice');
                    if (empty($invoicesGiven)) {
                        $invoice->load($invoice->getId());
                    }
                    $storeId = $invoice->getStoreId();
                    //force to print from an alternative store
                    if (isset($forceStoreId)) {
                        $storeId = $forceStoreId;
                    }
                    if ($storeId) {
                        $appEmulation = Mage::getSingleton('core/app_emulation');
                        $initial = $appEmulation->startEnvironmentEmulation(
                            $storeId, Mage_Core_Model_App_Area::AREA_FRONTEND, true
                        );
                    }

                    $invoiceHelper->setStoreId($storeId);
                    $invoiceHelper->setSalesObject($invoice);
                    $invoiceHelper->setPdf($pdf);
                    $pdf->setStoreId($storeId);
                    $pdf->setPdfHelper($invoiceHelper);
                    // set standard pdf info
                    $pdf->SetStandard($invoiceHelper);

                    // add a new page
                    $pdf->setIncrementId($invoice->getIncrementId());
                    $printedIncrements[]= $invoice->getIncrementId();
                    if ($i == 0) {
                        $pdf->AddPage();
                    } else {
                        $pdf->startPage();
                    }

                    // Print the logo
                    if ($invoiceHelper->getPrintBarcode()) {
                        $pdf->printHeader($invoiceHelper, $invoiceHelper->getPdfTitle(), $order->getIncrementId(), $hideLogo);
                    } else {
                        $pdf->printHeader($invoiceHelper, $invoiceHelper->getPdfTitle(), false, $hideLogo);

                    }

                    // Prepare Line Items
                    $pdf->prepareLineItems($invoiceHelper, $invoice, $order);

                    // Prepare Top
                    $topTemplate = $invoiceHelper->getTemplateFileWithPath(
                        $invoiceHelper,
                        'top',
                        self::PDFCUSTOMISER_PDF_TYPE
                    );
                    $top = Mage::app()->getLayout()->createBlock('pdfcustomiser/pdf_block')
                        ->setPdf($pdf)
                        ->setPdfHelper($invoiceHelper)
                        ->setTemplate($topTemplate)
                        ->toHtml();

                    $processor = Mage::helper('cms')->getBlockTemplateProcessor();
                    $processor->setVariables(
                        array(
                            'order' => $order,
                            'sales_object' => $invoice,
                            'billing_address'=> $pdf->PrepareCustomerAddress($invoiceHelper, $order, 'billing'),
                            'shipping_address'=> $pdf->PrepareCustomerAddress($invoiceHelper, $order, 'shipping'),
                            'payment'=> $pdf->PreparePayment($invoiceHelper, $order, $invoice),
                            'shipping'=> nl2br($pdf->PrepareShipping($invoiceHelper, $order, $invoice))
                        )
                    );
                    $top = $processor->filter($top);

                    //Prepare Totals
                    $totals = $this->PrepareTotals($invoiceHelper, $invoice);

                    //Prepare Bottom
                    $bottomTemplate = $invoiceHelper->getTemplateFileWithPath(
                        $invoiceHelper,
                        'bottom',
                        self::PDFCUSTOMISER_PDF_TYPE
                    );
                    $bottom = Mage::app()->getLayout()->createBlock('pdfcustomiser/pdf_block')
                        ->setPdf($pdf)
                        ->setPdfHelper($invoiceHelper)
                        ->setTotals($totals)
                        ->setTemplate($bottomTemplate)
                        ->toHtml();
                    $processor->setVariables(
                        array(
                            'order'        => $order,
                            'sales_object' => $invoice
                        )
                    );
                    $bottom = $processor->filter($bottom);

                    //Prepare Items
                    $itemsTemplate = $invoiceHelper->getTemplateFileWithPath(
                        $invoiceHelper,
                        'items'
                    );
                    $items = Mage::app()->getLayout()->createBlock('pdfcustomiser/pdf_items')
                        ->setPdf($pdf)
                        ->setPdfHelper($invoiceHelper)
                        ->setTemplate($itemsTemplate)
                        ->toHtml();

                    // Price to words By Eric
                    function number_in_words($num = false)
                      {
                          $num = str_replace(array(',', ' '), '' , trim($num));
                          if(! $num) {
                              return false;
                          }
                          $num = (int) $num;
                          $words = array();
                          $list1 = array('', 'vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni', 'dešimt', 'vienuolika',
                              'dvylika', 'trylika', 'keturiolika', 'penkiolika', 'šešiolika', 'septyniolika', 'aštuoniolika', 'devyniolika'
                          );
                          $list2 = array('', 'dešimt', 'dvidešimt', 'trysdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiadešimt', 'septyniasdešimt', 'aštuoniasdešimt', 'devyniasdešimt', 'šimtas');
                          $list3 = array('', 'tūkstan', 'Mlijonas');
                          $num_length = strlen($num);
                          $levels = (int) (($num_length + 2) / 3);
                          $max_length = $levels * 3;
                          $num = substr('00' . $num, -$max_length);
                          $num_levels = str_split($num, 3);
                          for ($i = 0; $i < count($num_levels); $i++) {
                              $levels--;
                              $hundreds = (int) ($num_levels[$i] / 100);
                              $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' šimt' . ( $hundreds == 1 ? 'as' : 'ai' ) . ' ' : '');
                              $tens = (int) ($num_levels[$i] % 100);
                              $singles = '';
                              if ( $tens < 20 ) {
                                  $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
                              } else {
                                  $tens = (int)($tens / 10);
                                  $tens = ' ' . $list2[$tens] . ' ';
                                  $singles = (int) ($num_levels[$i] % 10);
                                  $singles = ' ' . $list1[$singles] . ' ';
                              }
                              $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ( $list3[$levels] == 1 ? 'tis' : 'čiai' ). '' : '' );
                          } //end for loop
                          $commas = count($words);
                          if ($commas > 1) {
                              $commas = $commas - 1;
                          }
                          return implode(' ', $words);
                      }
                    //

                    // Price to words cents
                    function number_in_words_cents($num = false)
                      {
                          $num = str_replace(array(',', ' '), '' , trim($num));
                          $num = (int) $num;
                          $words = array();
                          $list1 = array('vienas', 'du', 'trys', 'keturi', 'penki', 'šeši', 'septyni', 'aštuoni', 'devyni', 'dešimt', 'vienuolika',
                              'dvylika', 'trylika', 'keturiolika', 'penkiolika', 'šešiolika', 'septyniolika', 'aštuoniolika', 'devyniolika'
                          );
                          $list2 = array('','dešimt', 'dvidešimt', 'trisdešimt', 'keturiasdešimt', 'penkiasdešimt', 'šešiadešimt', 'septyniasdešimt', 'aštuoniasdešimt', 'devvniasdešimt', 'šimtas');
                          $num_length = strlen($num);
                          $levels = (int) (($num_length + 2) / 3);
                          $max_length = $levels * 3;
                          $num = substr('00' . $num, -$max_length);
                          $num_levels = str_split($num, 3);
                          for ($i = 0; $i < count($num_levels); $i++) {
                              $levels--;
                              $tens = (int) ($num_levels[$i] % 100);
                              $singles = '';
                              if ( $tens < 20 ) {
                                  $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
                              } else {
                                  $tens = (int)($tens / 10);
                                  $tens = ' ' . $list2[$tens] . ' ';
                                  $singles = (int) ($num_levels[$i] % 10);
                                  $singles = ' ' . $list1[$singles] . ' ';
                              }
                              $words[] = $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ));
                          } //end for loop
                          $commas = count($words);
                          if ($commas > 1) {
                              $commas = $commas - 1;
                          }
                          if($num == 0){
                            $words[] = "nulis";
                          }
                          return implode(' ', $words);
                      }
                    //

                    //Put it all together
                    $pdf->writeHTML($top, false);
                    $pdf->SetFont($invoiceHelper->getPdfFont(), '', $invoiceHelper->getPdfFontsize('small'));
                    $pdf->writeHTML($items, false, false, false, false, '');
                    $pdf->SetFont($invoiceHelper->getPdfFont(), '', $invoiceHelper->getPdfFontsize());

                    // price to words by Eric
                    // explode comments
                    $total = $this->getTotal($invoiceHelper, $invoice);
                    $cents = explode(".",$total);
                    $pdf->writeHTML("<br /> <br />Suma žodžiais : ".number_in_words($total).
                      "&#8364; <br /> ir"
                    . number_in_words_cents($cents[1])." ct", false, false, false, false, '');
                    //

                    //reset Margins in case there was a page break
                    $pdf->setMargins($invoiceHelper->getPdfMargins('sides'), $invoiceHelper->getPdfMargins('top'));
                    $pdf->writeHTML($bottom, false);

                    /*
                    //Uncomment this block: delete /* and * /
                    //to add legal text for German invoices. EuVat Extension erforderlich
                    switch($order->getCustomerGroupId()){
                        case 2:
                            $pdf->Cell(0, 0, 'steuerfrei nach § 4 Nr. 1 b UStG', 0, 2, 'L',null,null,1);
                            break;
                        case 1:
                            $pdf->Cell(0, 0, 'umsatzsteuerfreie Ausfuhrlieferung', 0, 2, 'L',null,null,1);
                            break;
                    }
                     */

                    //print extra addresses for peel off labels
                    if ($invoiceHelper->getPdfIntegratedLabels()) {
                        $pdf->OutputCustomerAddresses($invoiceHelper, $order, $invoiceHelper->getPdfIntegratedLabels());
                    }
                    $pdf->endPage();
                    if ($storeId) {
                        $appEmulation->stopEnvironmentEmulation($initial);
                    }
                    $pdf->setPdfAnyOutput(true);
                }
            }
        }

        //output PDF document
        if (!$suppressOutput) {
            if ($pdf->getPdfAnyOutput()) {
                // reset pointer to the last page
                $pdf->lastPage();
                $pdf->Output(
                    $invoiceHelper->getPdfFileName($printedIncrements),
                    $invoiceHelper->getNewWindow()
                );
                exit;
            } else {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('adminhtml')->__('There are no printable documents related to selected orders')
                );
            }
        }


        $this->_afterGetPdf();

        return $pdf;
    }

}
