<?php

class Ansa_Catalog_Model_Category extends Mage_Catalog_Model_Abstract {

    public function getResizedImage($width, $height = null, $quality = 100) {
        if (!$this->getThumbnail())
            return false;

        $imageUrl = Mage::getBaseDir('media') . DS . "catalog" . DS . "category" . DS . $this->getThumbnail();
        if (!is_file($imageUrl))
            return false;

        if (!file_exists("./media/catalog/product/cache/cat_resized"))
            mkdir("./media/catalog/product/cache/cat_resized", 0777);

        $imageResized = Mage::getBaseDir('media') . DS . "catalog" . DS . "product" . DS . "cache" . DS . "cat_resized" . DS . $this->getThumbnail();
        if (!file_exists($imageResized) && file_exists($imageUrl) || file_exists($imageUrl) && filemtime($imageUrl) > filemtime($imageResized)):
            $imageObj = new Varien_Image($imageUrl);
            $imageObj->constrainOnly(true);
            $imageObj->keepAspectRatio(true);
            $imageObj->keepFrame(false);
            $imageObj->quality($quality);
            $imageObj->resize($width, $height);
            $imageObj->save($imageResized);
        endif;

        if (file_exists($imageResized)) {
            return Mage::getBaseUrl('media') . "/catalog/product/cache/cat_resized/" . $this->getThumbnail();
        } else {
            return $this->getThumbnail();
        }
    }

//    public function getThumbnailUrl($fullpath = false) {
//
//        $url = false;
//
//        if ($image = $this->getThumbnail()) {
//
//            if ($fullpath == true) {
//                $url = Mage::getBaseUrl('media') . 'catalog/category/' . $image;
//            } else {
//                $url = $image;
//            }
//        }
//
//        return $url;
//    }
}

?>
