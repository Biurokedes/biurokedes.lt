<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */

class Customy_BundleFields_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option extends Mage_Bundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Bundle_Option
{
    public function __construct()
    {
        $this->setTemplate('bundlefields/product/edit/bundle/option.phtml');
    }

    public function getImageSelectHtml($default = false)
    {
    	if ($default) {
    		$config = array(
    			'id' => $this->getFieldId().'_{{index}}_default_image',
    			'name' 	=> $this->getFieldName().'[{{index}}][default_image]',
    			'class' => 'select select-product-option-image',
    			'image'	=> '{{default_image}}',
    		);
		} else {
			$config = array(
				'id' 	=> $this->getFieldId().'_{{index}}_image',
    			'name' 	=> $this->getFieldName().'[{{index}}][image]',
    			'class' => 'select select-product-option-image',
				'image'	=> '{{image}}',
    		);
		}
		
		$options = $this->getOptions();
				
		// TODO: make class for generate element
		// build file field
		$image = '<input type="file" class="input-file ' . $config['class'] . '" value="" name="file_' . $config['name'] . '" id="file_' . $config['id'] . '">';
		$image .= '<input type="hidden" value="' . $config['image'] . '" name="' . $config['name'] . '" id="' . $config['id'] . '">';
		
		return $image;
    }
    
    public function getIsHiddenSelectHtml($default = false)
    {
    	if ($default) {
    		$config = array(
    			'id' => $this->getFieldId().'_{{index}}_default_is_hidden',
    			'name' 	=> $this->getFieldName().'[{{index}}][default_is_hidden]',
    			'class' => 'select select-product-option'
    		);
		} else {
			$config = array(
				'id' 	=> $this->getFieldId().'_{{index}}_is_hidden',
    			'name' 	=> $this->getFieldName().'[{{index}}][is_hidden]',
    			'class' => 'select select-product-option'
    		);
		}
		
		$options = $this->getOptions();
				
		// TODO: make class for generate element
		// build select field
		$select = '<select class="' . $config['class'] . '" value="" name="' . $config['name'] . '" id="' . $config['id'] . '">';
        $select .= '<option value="0">' . Mage::helper('bundlefields')->__('No') . '</option>';
        $select .= '<option value="1">' . Mage::helper('bundlefields')->__('Yes') . '</option>';
        $select .= '</select>';
		
		return $select;
    }

}
