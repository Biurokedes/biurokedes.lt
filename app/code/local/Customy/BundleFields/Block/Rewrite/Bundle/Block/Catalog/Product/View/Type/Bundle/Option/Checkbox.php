<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */
class Customy_BundleFields_Block_Rewrite_Bundle_Block_Catalog_Product_View_Type_Bundle_Option_Checkbox
    extends  Mage_Bundle_Block_Catalog_Product_View_Type_Bundle_Option_Checkbox
{
    /**
     * Set template
     *
     * @return void
     */
    protected function _construct()
    {
        if (Mage::getStoreConfig('bundlefields/options/active')){
            $this->setTemplate('bundlefields/bundle/catalog/product/view/type/bundle/option/checkbox.phtml');
        }else{
            $this->setTemplate('bundle/catalog/product/view/type/bundle/option/checkbox.phtml');
        }
    }
}
