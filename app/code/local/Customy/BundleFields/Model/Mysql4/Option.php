<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */

class Customy_BundleFields_Model_Mysql4_Option extends Mage_Bundle_Model_Mysql4_Option
{

    /**
     * After save process
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Bundle_Model_Mysql4_Option
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        parent::_afterSave($object);

        $condition = $this->_getWriteAdapter()->quoteInto('option_id = ?', $object->getId());
        $condition .= ' and (' . $this->_getWriteAdapter()->quoteInto('store_id = ?', $object->getStoreId());
        $condition .= ' or store_id = 0)';

        $this->_getWriteAdapter()->delete($this->getTable('option_value'), $condition);

        $data = new Varien_Object();
        $data->setOptionId($object->getId())
            ->setStoreId($object->getStoreId())
            ->setTitle($object->getTitle())
            ->setDescription($object->getDescription())
            ->setImage($object->getImage())
            ->setIsHidden($object->getIsHidden());

        $this->_getWriteAdapter()->insert($this->getTable('option_value'), $data->getData());

        /**
         * also saving default value if this store view scope
         */

        if ($object->getStoreId()) {
            $data->setStoreId('0');
            $data->setTitle($object->getDefaultTitle())
	            ->setDescription($object->getDefaultDescription())
	            ->setImage($object->getDefaultImage())
	            ->setIsHidden($object->getDefaultIsHidden());
            $this->_getWriteAdapter()->insert($this->getTable('option_value'), $data->getData());
        }

        return $this;
    }

}
