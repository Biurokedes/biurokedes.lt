<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */

class Customy_BundleFields_Model_Mysql4_Option_Collection extends Mage_Bundle_Model_Mysql4_Option_Collection
{

    public function joinValues($storeId)
    {
        $this->getSelect()->joinLeft(array('option_value_default' => $this->getTable('bundle/option_value')),
                '`main_table`.`option_id` = `option_value_default`.`option_id` and `option_value_default`.`store_id` = "0"',
                array())
            ->columns(array('default_title' => 'option_value_default.title'))
            ->columns(array('default_description' => 'option_value_default.description'))
            ->columns(array('default_image' => 'option_value_default.image'))
            ->columns(array('default_is_hidden' => 'option_value_default.is_hidden'));

        if ($storeId !== null) {
            $this->getSelect()
                ->columns(array('title' => 'IFNULL(`option_value`.`title`, `option_value_default`.`title`)'))
                ->columns(array('description' => 'IFNULL(`option_value`.`description`, `option_value_default`.`description`)'))
                ->columns(array('image' => 'IFNULL(`option_value`.`image`, `option_value_default`.`image`)'))
                ->columns(array('is_hidden' => 'IFNULL(`option_value`.`is_hidden`, `option_value_default`.`is_hidden`)'))
                ->joinLeft(array('option_value' => $this->getTable('bundle/option_value')),
                    '`main_table`.`option_id` = `option_value`.`option_id` and `option_value`.`store_id` = "' . $storeId . '"',
                    array());
        }
        return $this;
    }

}
