<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */
class Customy_BundleFields_Model_Observer
{

	public function saveBundleImage($observer) 
	{
		
        // save images
		if (!empty($_FILES['file_bundle_options']) && is_array($_FILES['file_bundle_options']['name'])) {
			$files = array();

			foreach ($_FILES['file_bundle_options']['name'] as $num => $name) {
				if ($_FILES['file_bundle_options']['error'][$num]['image'] == UPLOAD_ERR_OK) {
					$files[] = array(
						'tmp_name' 	=> $_FILES['file_bundle_options']['tmp_name'][$num]['image'],
						'type' 		=> $_FILES['file_bundle_options']['type'][$num]['image'],
						'name'		=> $name['image'],
						'error'		=> $_FILES['file_bundle_options']['error'][$num]['image'],
						'num'		=> $num,
					);
				}
			}

			if (count($files)) {
				$path = Mage::getBaseDir('media') . DS . 'bundlefieldsmedia';
				foreach ($files as $fileData) {

					$uploader = new Varien_File_Uploader($fileData);
					
		       		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(true);
					$uploader->setFilesDispersion(true);
				
					//Mage::log($fileData);
                    $uploader->save($path, $fileData['name']);
					
					// save filename to database - mage_bundle will do this... 
					// TODO make normal way to save image path
					//$_POST['bundle_options'][ $fileData['num'] ]['image'] = DS . 'media' . DS . 'otbundlefields' . $uploader->getUploadedFileName();
                    
                    $_POST['bundle_options'][ $fileData['num'] ]['image'] = 'bundlefieldsmedia' . $uploader->getUploadedFileName();                    
                    
				}
			}
		}
		
		// delete images
		$imageDelete = $observer->getRequest()->getParam('bundle_img_delete', array());
		if (count($imageDelete)) {
			foreach ($imageDelete as $num => $value) {
				$filename = "." . $_POST['bundle_options'][ $num ]['image'];
				if (file_exists($filename)) {
					unlink($filename);					
				}
				$_POST['bundle_options'][ $num ]['image'] = '';
			}
		}
	}
	
}
