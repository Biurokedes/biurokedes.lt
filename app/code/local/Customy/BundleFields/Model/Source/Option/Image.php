<?php
/**
 * Customy
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Customy EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.customy.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@customy.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.customy.com/ for more information
 * or send an email to sales@customy.com
 *
 * @copyright  Copyright (c) 2011-2013 Orot Technologies s.r.o.
 * @license    http://www.customy.com/LICENSE-1.0.html
 */

class Customy_BundleFields_Model_Source_Option_Image
{

    public function toOptionArray()
    {
    	$path = Mage::getBaseDir('media').DS.'bundle_options'.DS;
        $options = array(
        	array(
        		'label' => 'No Image',
        		'value' => ''
        	)
        );
		
		$dh = opendir($path);
		while ($file = readdir($dh)) {
			if (!is_dir($path.$file)) {
				$name = explode('.', $file);
				$options[] = array (
					'label' => $name[0],
					'value' => $file
				);
			}
		}
		closedir($dh);
        return $options;
    }
}
