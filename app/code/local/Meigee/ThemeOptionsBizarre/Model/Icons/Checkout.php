<?php
/**
 * Magento
 *
 * @author    Meigeeteam http://www.meaigeeteam.com <nick@meaigeeteam.com>
 * @copyright Copyright (C) 2010 - 2012 Meigeeteam
 *
 */
class Meigee_ThemeOptionsBizarre_Model_Icons_Checkout
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'fa-usd', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-usd')),
            array('value'=>'fa-money', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-money')),
			array('value'=>'fa-credit-card', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-credit-card')),
            array('value'=>'fa-check-square-o', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-check-square-o')),
            array('value'=>'fa-check-square', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-check-square')),
			array('value'=>'fa-list-alt', 'label'=>Mage::helper('ThemeOptionsBizarre')->__('fa-list-alt'))
        );
    }

}