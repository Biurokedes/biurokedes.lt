<?php
class Meigee_ThemeOptionsBizarre_Model_Observer
{
	/**
	* Call actions after any system config is saved
	*/
	public function cssgenerate()
	{
		$section = Mage::app()->getRequest()->getParam('section');

		if ($section == 'meigee_bizarre_design' || $section == 'meigee_bizarre_design_skin1' || $section == 'meigee_bizarre_design_skin2' || $section == 'meigee_bizarre_design_skin3' || $section == 'meigee_bizarre_design_skin4' || $section == 'meigee_bizarre_design_skin5' || $section == 'meigee_bizarre_design_skin6' || $section == 'meigee_bizarre_design_skin7' || $section == 'meigee_bizarre_design_skin8')
		{
			Mage::getSingleton('ThemeOptionsBizarre/Cssgenerate')->saveCss();
			$response = Mage::app()->getFrontController()->getResponse();
			$response->sendResponse();
		}

	}
	
	
	/**
	* After customer_login
	*/
	public function loginredirect($observer){
		if(!Mage::getStoreConfig('customer/startup/redirect_dashboard')){
			$pageurl = Mage::app()->getRequest()->getParam('pageurl');
			if($pageurl){
				Mage::app()->getResponse()->setRedirect($pageurl)->sendResponse();
				exit;
			}
		}
	}
}
