<?php
/**
 * Magento
 *
 * @author    Meigeeteam http://www.meigeeteam.com <nick@meigeeteam.com>
 * @copyright Copyright (C) 2010 - 2014 Meigeeteam
 *
 */
class Meigee_ThemeOptionsBizarre_MoreviewsController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$productId  = (int) $this->getRequest()->getParam('product'); //get parameter from url request
		// Prepare helper and params
		$viewHelper = Mage::helper('catalog/product_view');
		$params = new Varien_Object();
		$params->setCategoryId(false);
		$params->setSpecifyOptions(false);
		$viewHelper->prepareAndRender($productId, $this, $params);
    }
}