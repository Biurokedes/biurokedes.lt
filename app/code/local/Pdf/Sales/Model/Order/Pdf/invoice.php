<?php 
	
	class Pdf_Sales_Model_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Invoice
{
    public function insertDocumentNumber(Zend_Pdf_Page $page, $text)
    {
        $number = substr($text, strrpos($text, ' ')+1);
        $x = strpos($number, '0');
        $root = substr($number, 0, $x);
        $base = substr($number, $x);
        parent::insertDocumentNumber($page, 'Number: ' . $root . '/' . $base);
    }
}