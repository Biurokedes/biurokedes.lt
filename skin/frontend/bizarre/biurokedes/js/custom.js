(function ($) {
    $(document).ready(function ($) {
        $(".options-list li").tooltip({
            items: "label",
            content: function () {
                var tooltip_image = $(this).parent().parent().find('.tooltip-image').html();
                return tooltip_image;
            }
        });

        $(".count").tooltip({
            items: ".product-title span",
            content: function () {
                var tooltip_image = $(this).parent().find('.tooltip-image').html();
                return tooltip_image;
            }
        });

        $("dd.option-item").tooltip({
            items: "label",
            content: function () {
                var tooltip_image = $(this).parent().find('.tooltip-image').html();
                return tooltip_image;
            }
        });
        $("dd.options-items").tooltip({
            items: "label",
            content: function () {
                var tooltip_image = $(this).parent().find('.tooltip-image').html();
                return tooltip_image;
            }
        });
        $(".parameters").tooltip({
            items: "img",
            content: function () {
                var tooltip_image = $(this).next('.tooltip-image').html();
                return tooltip_image;
            }
        });


        $('dt .show-desc').click(function () {
            var parent = $(this).parent();
            var neighbour = $(parent).next();
            $(neighbour).find('.bundle-description').toggle();
        });

        //$('.hide-desc').click(function () {
            //var parent = $(this).parent();
            ////var neighbour = $(parent).next();
            //parent.toggle();
            //$("html, body").animate({scrollTop: 0}, "slow");
        //});

        // test

        //Chair parameters
        $('.parameters .show-desc').click(function () {
            var parent = $(this).parent();
            $(parent).find('.parameters-desc').toggle();
        });


        $('li.option-item.hidden').parent().parent().find('h3').addClass('hidden');

        //console.log($('#product-options-wrapper').scrollTop());
        if ($('.additional-fields').length || $('.upholstery li').length > 1) {
            $('.show-more-options').toggle();
        }

        $('.show-more-options').click(function () {
			$('.upholstery > li:not(:first-child)').toggleClass('hidden', 700 , 'linear');
			//$('.upholstery > li:not(:first-child)').toggle();
            $('.hide-text, .show-text').toggle();
            $('li.option-item.hide').parent().parent().find('h3').toggleClass('hidden', 700, 'linear');
            console.log($('li.option-item.hide').parent().parent().find('h3').text());
            $('.upholstery-color.hide').toggleClass('hidden', 700, 'linear');
        });


		function expandOptions(event) {
			var pageOffset = pageYOffset;
				$('html, body').delay(700).animate({scrollTop: pageYOffset + 300}, 1000).promise().then(function(){
					$(event.data.element).one("click", {element: event.data.element, pageOffset: pageOffset} , collapseOptions);
				});
		}
		function collapseOptions(event) {
				$('html, body').animate({scrollTop: event.data.pageOffset}, 1000).promise().then(function(){
					$(event.data.element).one("click", {element: '.show-more-options'}, expandOptions);
				});
		}
		$('.show-more-options').one("click", {element: '.show-more-options'}, expandOptions);

        $('.parameters .hide-desc').click(function () {
            var pageOffset = $('.parameters').offset().top;
            var parentElement = $(this).parent();
			$('html, body').animate({scrollTop: pageOffset - 70 }, 1000).promise().then(function(){
            	parentElement.hide();
        	});
        });
    }); // end of document.ready
})(jQuery);
