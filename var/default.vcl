C{
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
static pthread_mutex_t lrand_mutex = PTHREAD_MUTEX_INITIALIZER;
void generate_uuid(char* buf) {
pthread_mutex_lock(&lrand_mutex);
long a = lrand48();
long b = lrand48();
long c = lrand48();
long d = lrand48();
pthread_mutex_unlock(&lrand_mutex);
sprintf(buf, "frontend=%08lx%04lx%04lx%04lx%04lx%08lx",
a,
b & 0xffff,
(b & ((long)0x0fff0000) >> 16) | 0x4000,
(c & 0x0fff) | 0x8000,
(c & (long)0xffff0000) >> 16,
d
);
return;
}
}C
backend default {
.host = "5.199.164.106";
.port = "81";
.first_byte_timeout = 300s;
.between_bytes_timeout = 300s;
}
backend admin {
.host = "5.199.164.106";
.port = "81";
.first_byte_timeout = 21600s;
.between_bytes_timeout = 21600s;
}
acl crawler_acl {
"127.0.0.1";
}
acl debug_acl {
}
sub generate_session {
if (req.url ~ ".*[&?]SID=([^&]+).*") {
set req.http.X-Varnish-Faked-Session = regsub(
req.url, ".*[&?]SID=([^&]+).*", "frontend=\1");
} else {
C{
char uuid_buf [50];
generate_uuid(uuid_buf);
VRT_SetHdr(sp, HDR_REQ,
"\030X-Varnish-Faked-Session:",
uuid_buf,
vrt_magic_string_end
);
}C
}
if (req.http.Cookie) {
set req.http.Cookie = req.http.X-Varnish-Faked-Session "; " req.http.Cookie;
} else {
set req.http.Cookie = req.http.X-Varnish-Faked-Session;
}
}
sub generate_session_expires {
C{
time_t now = time(NULL);
struct tm now_tm = *localtime(&now);
now_tm.tm_sec += 3600;
mktime(&now_tm);
char date_buf [50];
strftime(date_buf, sizeof(date_buf)-1, "%a, %d-%b-%Y %H:%M:%S %Z", &now_tm);
VRT_SetHdr(sp, HDR_RESP,
"\031X-Varnish-Cookie-Expires:",
date_buf,
vrt_magic_string_end
);
}C
}
sub vcl_recv {
if (req.restarts == 0) {
if (req.http.X-Forwarded-For) {
set req.http.X-Forwarded-For =
req.http.X-Forwarded-For ", " client.ip;
} else {
set req.http.X-Forwarded-For = client.ip;
}
}
set req.http.X-Opt-Enable-Caching = "true";
set req.http.X-Opt-Force-Static-Caching = "true";
set req.http.X-Opt-Enable-Get-Excludes = "true";
if (req.http.X-Opt-Enable-Caching != "true" || req.http.Authorization ||
!(req.request ~ "^(GET|HEAD)$") ||
req.http.Cookie ~ "varnish_bypass=1") {
return (pipe);
}
set req.url = regsuball(req.url, "(.*)//+(.*)", "\1/\2");
if (req.http.Accept-Encoding) {
if (req.http.Accept-Encoding ~ "gzip") {
set req.http.Accept-Encoding = "gzip";
} else if (req.http.Accept-Encoding ~ "deflate") {
set req.http.Accept-Encoding = "deflate";
} else {
unset req.http.Accept-Encoding;
}
}
if (req.url ~ "^(/english3/|/english5/|/english6/|/english4/|/english2/|/spanish/|/english/|/german/|/media/|/skin/|/js/)(?:(?:index|litespeed)\.php/)?") {
set req.http.X-Turpentine-Secret-Handshake = "1";
if (req.url ~ "^(/english3/|/english5/|/english6/|/english4/|/english2/|/spanish/|/english/|/german/|/media/|/skin/|/js/)(?:(?:index|litespeed)\.php/)?iejimas") {
set req.backend = admin;
return (pipe);
}
if (req.http.Cookie ~ "\bcurrency=") {
set req.http.X-Varnish-Currency = regsub(
req.http.Cookie, ".*\bcurrency=([^;]*).*", "\1");
}
if (req.http.Cookie ~ "\bstore=") {
set req.http.X-Varnish-Store = regsub(
req.http.Cookie, ".*\bstore=([^;]*).*", "\1");
}
if (req.url ~ "/turpentine/esi/get(?:Block|FormKey)/") {
set req.http.X-Varnish-Esi-Method = regsub(
req.url, ".*/method/(\w+)/.*", "\1");
set req.http.X-Varnish-Esi-Access = regsub(
req.url, ".*/access/(\w+)/.*", "\1");
remove req.http.Accept-Encoding;
}
if (req.http.Cookie !~ "frontend=") {
if (client.ip ~ crawler_acl ||
req.http.User-Agent ~ "^(?:ApacheBench/.*|.*Googlebot.*|JoeDog/.*Siege.*|magespeedtest\.com|Nexcessnet_Turpentine/.*)$") {
set req.http.Cookie = "frontend=crawler-session";
} else {
call generate_session;
}
}
if (req.http.X-Opt-Force-Static-Caching == "true" &&
req.url ~ ".*\.(?:css|js|jpe?g|png|gif|ico|swf)(?=\?|&|$)") {
remove req.http.Cookie;
remove req.http.X-Varnish-Faked-Session;
return (lookup);
}
if (req.url ~ "^(/english3/|/english5/|/english6/|/english4/|/english2/|/spanish/|/english/|/german/|/media/|/skin/|/js/)(?:(?:index|litespeed)\.php/)?(?:iejimas|api|cron\.php)" ||
req.url ~ "\?.*__from_store=") {
return (pipe);
}
if (req.http.X-Opt-Enable-Get-Excludes == "true" &&
req.url ~ "(?:[?&](?:__SID|XDEBUG_PROFILE)(?=[&=]|$))") {
return (pass);
}
if (req.url ~ "[?&](utm_source|utm_medium|utm_campaign|gclid|cx|ie|cof|siteurl)=") {
set req.url = regsuball(req.url, "(?:(\?)?|&)(?:utm_source|utm_medium|utm_campaign|gclid|cx|ie|cof|siteurl)=[^&]+", "\1");
set req.url = regsuball(req.url, "(?:(\?)&|\?$)", "\1");
}
return (lookup);
}
}
sub vcl_pipe {
remove bereq.http.X-Turpentine-Secret-Handshake;
set bereq.http.Connection = "close";
}
sub vcl_hash {
set req.hash += req.url;
if (req.http.Host) {
set req.hash += req.http.Host;
} else {
set req.hash += server.ip;
}
set req.hash += req.http.Ssl-Offloaded;
if (req.http.X-Normalized-User-Agent) {
set req.hash += req.http.X-Normalized-User-Agent;
}
if (req.http.Accept-Encoding) {
set req.hash += req.http.Accept-Encoding;
}
if (req.http.X-Varnish-Store || req.http.X-Varnish-Currency) {
set req.hash += "s=";
set req.hash += req.http.X-Varnish-Store;
set req.hash += "&c=";
set req.hash += req.http.X-Varnish-Currency;
}
if (req.http.X-Varnish-Esi-Access == "private" &&
req.http.Cookie ~ "frontend=") {
set req.hash += regsub(req.http.Cookie, "^.*?frontend=([^;]*);*.*$", "\1");
}
return (hash);
}
sub vcl_fetch {
set req.grace = 15s;
set beresp.http.X-Varnish-Host = req.http.host;
set beresp.http.X-Varnish-URL = req.url;
if (req.url ~ "^(/english3/|/english5/|/english6/|/english4/|/english2/|/spanish/|/english/|/german/|/media/|/skin/|/js/)(?:(?:index|litespeed)\.php/)?") {
remove beresp.http.Vary;
if (beresp.status != 200 && beresp.status != 404) {
set beresp.ttl = 15s;
return (pass);
} else {
if (beresp.http.Set-Cookie) {
set beresp.http.X-Varnish-Set-Cookie = beresp.http.Set-Cookie;
remove beresp.http.Set-Cookie;
}
remove beresp.http.Cache-Control;
remove beresp.http.Expires;
remove beresp.http.Pragma;
remove beresp.http.Cache;
remove beresp.http.Age;
if (beresp.http.X-Turpentine-Esi == "1") {
esi;
}
if (beresp.http.X-Turpentine-Cache == "0") {
set beresp.cacheable = false;
set beresp.ttl = 15s;
return (pass);
} else {
set beresp.cacheable = true;
if (req.http.X-Opt-Force-Static-Caching == "true" &&
bereq.url ~ ".*\.(?:css|js|jpe?g|png|gif|ico|swf)(?=\?|&|$)") {
set beresp.ttl = 28800s;
set beresp.http.Cache-Control = "max-age=28800";
} else if (req.http.X-Varnish-Esi-Method) {
if (req.http.X-Varnish-Esi-Access == "private") {
if (req.http.Cookie ~ "frontend=") {
set beresp.http.X-Varnish-Session = regsub(req.http.Cookie,
"^.*?frontend=([^;]*);*.*$", "\1");
}
if (req.http.X-Varnish-Esi-Method == "ajax") {
set beresp.ttl = 15s;
return (pass);
} else {
set beresp.ttl = 3600s;
}
} else {
if (req.http.X-Varnish-Esi-Method == "ajax") {
set beresp.http.Cache-Control =
"max-age=3600";
}
set beresp.ttl = 3600s;
}
} else {
set beresp.ttl = 3600s;
}
}
}
return (deliver);
}
}
sub vcl_deliver {
if (req.http.X-Varnish-Faked-Session) {
call generate_session_expires;
set resp.http.Set-Cookie = req.http.X-Varnish-Faked-Session "; expires="
resp.http.X-Varnish-Cookie-Expires "; path=/";
if (req.http.Host) {
set resp.http.Set-Cookie = resp.http.Set-Cookie
"; domain=" regsub(req.http.Host, ":\d+$", "");
}
set resp.http.Set-Cookie = resp.http.Set-Cookie "; HttpOnly";
remove resp.http.X-Varnish-Cookie-Expires;
}
if (req.http.X-Varnish-Esi-Method == "ajax" && req.http.X-Varnish-Esi-Access == "private") {
set resp.http.Cache-Control = "no-cache";
}
set resp.http.X-Opt-Debug-Headers = "false";
if (resp.http.X-Opt-Debug-Headers == "true" || client.ip ~ debug_acl ) {
set resp.http.X-Varnish-Hits = obj.hits;
set resp.http.X-Varnish-Esi-Method = req.http.X-Varnish-Esi-Method;
set resp.http.X-Varnish-Esi-Access = req.http.X-Varnish-Esi-Access;
set resp.http.X-Varnish-Currency = req.http.X-Varnish-Currency;
set resp.http.X-Varnish-Store = req.http.X-Varnish-Store;
} else {
remove resp.http.X-Varnish;
remove resp.http.Via;
remove resp.http.X-Powered-By;
remove resp.http.Server;
remove resp.http.X-Turpentine-Cache;
remove resp.http.X-Turpentine-Esi;
remove resp.http.X-Turpentine-Flush-Events;
remove resp.http.X-Turpentine-Block;
remove resp.http.X-Varnish-Session;
remove resp.http.X-Varnish-Host;
remove resp.http.X-Varnish-URL;
remove resp.http.X-Varnish-Set-Cookie;
}
remove resp.http.X-Opt-Debug-Headers;
}